# import libraries that are needed
import numpy as np

import scipy.io

import matplotlib.pyplot as plt
import matplotlib.image as mpimg



# Load in the data
mat = scipy.io.loadmat('Mouse_Brain2013.mat')

# Some information about the dataset.
# 
# Nb of rows (y_size) = 34
# Nb of columns (x_size) = 51
# Nb of total pixels = 1734 (total number of pixels including the null ones (zero pixels))
# Nb of bins for each pixel = 6490 (is the same for all pixels)
# Nb of non-zero datapoints = 1381 (non-zero pixels)
#
# Mouse_Brain2013.mat: (Contains the raw non-zero data of the dataset)
#   y_size: Number of rows in the grid (= 34)
#   x_size: Number of columns in the grid (= 51)
#   mz_values: mz-values (6490) (Ignore for now)
#   row2grid: Matrix with 1734 rows and 2 columns. Contains x,y indices over the grid. 
#             Used for mapping 1D list of pixels (ims_bn) to 2D matrix.
#   grid2row: Holds indices (51) of the pixels corresponding to each row. 
#             Each cell holds 34 indices.
#   ims_bn: Matrix of 6490 rows and 1381 columns. Each row corresponds to a mz bin, 
#           each column corresponds to a pixel. Only non-zero datapoins. This is the data
#           that you will need!
#   spatial_rows: Ignore
#   nonnull_spots: Holds a list of 1734 boolean variables which are 1 if the pixel 
#                  at that index corresponds to real tissue.

# Explore some of the variables in the dataset
# Number of rows
y_size = np.int32(mat['y_size'][0][0])
# Number of columns
x_size = np.int32(mat['x_size'][0][0])
# Number of total pixels
nb_pixels = y_size * x_size
# Number of bins
feat_size = len(mat['mz_values'][0])

# mapping from pixel index to row-col
row2grid = mat.get('row2grid')
# Baseline normalised non-zero pixels
ims_bn = mat.get('ims_bn')
# Number of nonzero pixels
nb_nonzero_pix = ims_bn.shape[1]
# List of booleans telling if the pixel is zero or not
nonnull_spots  = mat['nonnull_spots']

print 'y_size: ', y_size
print 'x_size: ', x_size
print 'nb_pixels:  ', nb_pixels
print 'nb_nonzero_pix: ', nb_nonzero_pix
print 'feat_size: ', feat_size
print 'row2grid.shape: ', row2grid.shape
print 'ims_bn.shape: ', ims_bn.shape
print 'nonnull_spots: ', nonnull_spots.shape

print row2grid


# Define a function to display an image of a specific mass bin
def get_img(data, bin_nb):
    """
    Generate an image from a specific mz bin in the MSI dataset.
    The data rows have to correspond to pixels.
    The data columns have to correspond to mz bins.
    """
    img = np.ndarray(shape=(y_size, x_size), dtype=float)
    nonzero_pix_idx = 0
    for pix_idx in xrange(nb_pixels):
        y, x = row2grid[pix_idx] - 1
        if nonnull_spots[pix_idx]:
            img[x,y] = data[nonzero_pix_idx, bin_nb]
            nonzero_pix_idx += 1
        else:
            img[x,y] = 0.0
    return img


# Show a sample image
img = get_img(ims_bn.T, 500)
imgplot = plt.imshow(img)
plt.colorbar()